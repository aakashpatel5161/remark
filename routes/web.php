<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
ADMIN PANEL ROUTES GOES HERE WITH PREFIX admin
*/
/*Route::prefix('admin')->namespace('Admin')->group(function(){
    Route::get('/', ['as'=>'admin_login_path','uses'=>'LoginController@index']);
    Route::post('/',['as'=>'admin_login_check_path','uses'=>'LoginController@login']);
    Route::get('logout',['as'=>'admin_logout_path','uses'=>'LoginController@logout']);

    // Admin module
    Route::get('dashboard',['as'=>'admin_dashboard_path','middleware' => 'MyAdminPanelAuth','uses'=>'DashboardController@index']);
    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
});*/

Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'Admin\LoginController@index');
  Route::get('/reset', 'Admin\ResetController@index');
  Route::get('/dashboard', 'Admin\DashboardController@index');
  Route::get('/providers', 'Admin\ProvidersController@index');
  Route::get('/manage-users', function () {
      return view('admin.manageusers');  
    });
    Route::get('contact/all', function () {
        return view('admin.contactusers.sendtoall');
    });

    Route::get('contact/individually', function () {
        return view('admin.contactusers.sendindividually');
    });

    Route::get('contact/bycountry', function () {
        return view('admin.contactusers.sendbycountry');
    });

    Route::get('contact/bydate', function () {
        return view('admin.contactusers.sendbydate');
    });

    Route::get('edit', function () {
        return view('admin.edituser');
    });

    Route::get('edit-profile', function () {
        return view('admin.editprofile');
    });

    Route::get('change-password', function () {
        return view('admin.changepassword');
    });
});