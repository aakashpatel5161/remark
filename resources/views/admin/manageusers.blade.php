@extends('admin.master')
@section('title')
Manage Users
@endsection

@section('css')
<link rel="stylesheet" href="{!! asset('/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css') !!}">
<link rel="stylesheet" href="/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css">
<link rel="stylesheet" href="/examples/css/tables/datatable.css">
<link rel="stylesheet" href="/examples/css/uikit/modals.css">
@endsection


@section('content')
@include('admin.include.header')

<div class="page">
        <div class="page-content ">
              <!-- Panel Basic -->
        <div class="panel panel-primary panel-line">
            <header class="panel-heading">
              <div class="panel-actions"></div>
              <h3 class="panel-title">Manage Users</h3>
            </header>
            <div class="panel-body">
              <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Actions</th>
                  </tr>
                </tfoot>
                <tbody>
                  <tr>
                    <td>Damon</td>
                    <td>5516 Adolfo Green</td>
                    <td>Littelhaven</td>
                    <td>85</td>
                    <td>2014/06/13</td>
                    <td><div class="row ml-3">  
                      <button type="button" class="btn btn-icon btn-primary" data-target="#showmodal" data-toggle="modal"><i class="icon md-people" aria-hidden="true"></i></button>&nbsp;
                      <a href="/edit" class="btn btn-success p-2">
                        <i class="icon md-edit" aria-hidden="true"></i>
                     </a>&nbsp;
    
                      <button type="button" class="btn btn-icon btn-danger" data-target="#deletemodal" data-toggle="modal"><i class="icon md-delete" aria-hidden="true"></i></button>&nbsp;
                    </div></td>
                  </tr>
                  <tr>
                    <td>Torrey</td>
                    <td>1995 Richie Neck</td>
                    <td>West Sedrickstad</td>
                    <td>77</td>
                    <td>2014/09/12</td>
                    <td>$243,577</td>
                  </tr>
                  <tr>
                    <td>Miracle</td>
                    <td>176 Hirthe Squares</td>
                    <td>Ryleetown</td>
                    <td>82</td>
                    <td>2013/09/27</td>
                    <td>$784,802</td>
                  </tr>
                  <tr>
                    <td>Wilhelmine</td>
                    <td>44727 O&#x27;Hara Union</td>
                    <td>Dibbertfurt</td>
                    <td>68</td>
                    <td>2013/06/28</td>
                    <td>$207,291</td>
                  </tr>
                  <tr>
                    <td>Hubert</td>
                    <td>8884 Jamel Pines</td>
                    <td>Howemouth</td>
                    <td>63</td>
                    <td>2013/05/28</td>
                    <td>$584,032</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>        
        
        </div>
</div>

  <!-- Show Modal -->
    <div class="modal fade modal-3d-flip-horizontal" id="showmodal"
      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
      tabindex="-1">
      <div class="modal-dialog modal-simple">
          <form class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">User Profile</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-xl-5 form-group">

                  </div>
                  <div class="col-xl-7 form-group">
                      <b>Name :</b>
                      <h6 id="name"></h6>
                      <b>Email :</b>
                      <h6 id="email"></h6>
                      <b>Username :</b>
                      <h6 id="username"></h6>
                      <div class="row">
                          <div class="col-md-6">
                            <b>Date of Birth :</b>
                            <p id="dob"></p>
                          </div>
                          <div class="col-md-6">
                            <b>Gender :</b>
                            <p id="gender"></p>
                          </div>
                      </div>	
                      <div class="row">
                          <div class="col-sm-6">
                            <b>Country :</b>
                            <p id="country"></p>
                          </div>
                          <div class="col-sm-6">
                            <b>Phone :</b>
                            <p id="phone"></p>
                          </div>
                        </div>		
                  </div>
                </div>
              </div>
            </form>
    </div>
</div>
<!-- End Modal -->


<!-- Delete Modal -->
<div class="modal fade modal-3d-flip-horizontal" id="deletemodal"
aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
tabindex="-1">
<div class="modal-dialog modal-simple">
    <form class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="exampleFormModalLabel">Are you sure?</h4>
        </div>
        <div class="modal-body">
         <h5>Do you really want to delete the user?</h5>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger">Delete</button>
          </div>
      </form>
</div>
</div>
<!-- End Modal -->

@endsection

@section('js')
<script src="/global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
<script src="/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
<script src="/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
<script src="/global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
<script src="/global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="/global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.html5.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.flash.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.print.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
<script src="/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
<script src="/global/vendor/asrange/jquery-asRange.min.js"></script>
<script src="/global/vendor/bootbox/bootbox.js"></script>
<script src="/global/js/Plugin/datatables.js"></script>
<script src="/examples/js/tables/datatable.js"></script>
<script src="/examples/js/uikit/icon.js"></script>
@endsection
    
@section('footer')
@include('admin.include.footer')
@endsection