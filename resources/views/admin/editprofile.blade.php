@extends('admin.master')
@section('title')
Edit Profile
@endsection

@section('css')
<link rel="stylesheet" href="/examples/css/forms/layouts.css">
@endsection


@section('content')
@include('admin.include.header')

<div class="page">
        <div class="page-content">
                <div class="col-xxl-8 col-lg-10">
                        <div class="panel panel-primary panel-line">
                                <div class="panel-heading">
                                  <h3 class="panel-title">Edit Profile
                                  </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="example-wrap">                            
                                            <div class="example">
                                                <form autocomplete="off">
                                                    <div class="col-md-6 col-lg-4">
                                                        <div class="example-wrap">                                                                
                                                            <div class="form-group">
                                                                <div class="input-group input-group-file" data-plugin="inputGroupFile">
                                                                <input type="text" class="form-control" readonly="">
                                                                <span class="input-group-append">
                                                                    <span class="btn btn-success btn-file">
                                                                    <i class="icon md-upload" aria-hidden="true"></i>
                                                                    <input type="file" name="profileopic" multiple="">
                                                                    </span>
                                                                </span>
                                                                </div>
                                                            </div>                                                             
                                                            </div>
                                                    </div>
                                                    
                                                <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicEmail">Name</label>
                                                        <input type="email" class="form-control" id="name" name="name"
                                                        placeholder="Name" autocomplete="off" />
                                                </div>
                                                <div class="form-group form-material">
                                                    <label class="form-control-label">Gender</label>
                                                    <div>
                                                    <div class="radio-custom radio-default radio-inline">
                                                        <input type="radio" id="inputBasicMale" name="gender" />
                                                        <label for="inputBasicMale">Male</label>
                                                    </div>
                                                    <div class="radio-custom radio-default radio-inline">
                                                        <input type="radio" id="inputBasicFemale" name="gender" />
                                                        <label for="inputBasicFemale">Female</label>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-material">
                                                        <label class="form-control-label" for="Username">Username</label>
                                                        <input type="username" class="form-control" id="username" name="username"
                                                        placeholder="Username" autocomplete="off" />
                                                </div>
                                                <div class="form-group form-material">
                                                        <label class="form-control-label" for="phone">Phone</label>
                                                        <input type="phone" class="form-control" id="phone" name="phone"
                                                        placeholder="Phone" autocomplete="off" />
                                                </div>
                                                <div class="form-group form-material">
                                                    <label class="form-control-label" for="inputBasicEmail">Email</label>
                                                    <input type="email" class="form-control" id="email" name="email"
                                                    placeholder="Email" autocomplete="off" />
                                                </div>
                                                <div class="form-group form-material">
                                                    <label class="form-control-label" for="inputBasicPassword">Country</label>
                                                    <input type="country" class="form-control" id="country" name="country"
                                                    placeholder="Country" autocomplete="off" />
                                                </div>
                    
                                                <div class="form-group form-material">
                                                    <button type="button" class="btn btn-primary">Update</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                              <!-- End Example Basic Form -->
                                            </div>
                                </div>
                              </div>
                              <!-- End Example Heading With Desc -->
        </div>
</div>


@endsection

@section('js')
<script src="/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="/global/js/Plugin/jquery-placeholder.js"></script>
<script src="/global/js/Plugin/material.js"></script>
<script src="/global/vendor/asprogress/jquery-asProgress.js"></script>
<script src="/global/vendor/draggabilly/draggabilly.pkgd.js"></script>
<script src="/global/vendor/raty/jquery.raty.js"></script>
<script src="/global/js/Plugin/responsive-tabs.js"></script>
<script src="/global/js/Plugin/tabs.js"></script>
<script src="/global/js/Plugin/asprogress.js"></script>
<script src="/global/js/Plugin/panel.js"></script>
<script src="/global/js/Plugin/asscrollable.js"></script>
<script src="/global/js/Plugin/raty.js"></script>
<script src="/examples/js/uikit/panel-structure.js"></script>
<script src="/global/js/Plugin/input-group-file.js"></script>
@endsection
    
@section('footer')
@include('admin.include.footer')
@endsection