<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    
    <title>@yield('title')</title>

    <link rel="apple-touch-icon" href="{!! asset('/images/apple-touch-icon.png') !!}">
    <link rel="shortcut icon" href="{!! asset('/images/favicon.ico') !!}">
        
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{!! asset('/global/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/global/css/bootstrap-extend.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/css/site.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/skins/green.min.css') !!}">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="{!! asset('/global/vendor/animsition/animsition.css') !!}">
    <link rel="stylesheet" href="{!! asset('/global/vendor/asscrollable/asScrollable.css') !!}">
    <link rel="stylesheet" href="{!! asset('/global/vendor/switchery/switchery.css') !!}">
    <link rel="stylesheet" href="{!! asset('/global/vendor/intro-js/introjs.css') !!}">
    <link rel="stylesheet" href="{!! asset('/global/vendor/slidepanel/slidePanel.css') !!}">
    <link rel="stylesheet" href="{!! asset('/global/vendor/flag-icon-css/flag-icon.css') !!}">
    <link rel="stylesheet" href="{!! asset('/global/vendor/waves/waves.css') !!}">
    <link rel="stylesheet" href="{!! asset('/examples/css/pages/login-v3.css') !!}">

    <!-- Fonts -->
    <link rel="stylesheet" href="{!! asset('/global/fonts/material-design/material-design.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/global/fonts/brand-icons/brand-icons.min.css') !!}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <script src="{!! asset('/global/vendor/breakpoints/breakpoints.js') !!}"></script>
    <script>
      Breakpoints();
    </script>
    @yield('css')
</head>
<body class="animsition page-login-v3 layout-full">
    @yield('content')

    <!-- Core  -->
    <script src="{!! asset('/global/vendor/babel-external-helpers/babel-external-helpers.js') !!}"></script>
    <script src="{!! asset('/global/vendor/jquery/jquery.js') !!}"></script>
    <script src="{!! asset('/global/vendor/validate/jquery.validate.min.js') !!}"></script>
    <script src="{!! asset('/global/vendor/popper-js/umd/popper.min.js') !!}"></script>
    <script src="{!! asset('/global/vendor/bootstrap/bootstrap.js') !!}"></script>
    <script src="{!! asset('/global/vendor/animsition/animsition.js') !!}"></script>
    <script src="{!! asset('/global/vendor/mousewheel/jquery.mousewheel.js') !!}"></script>
    <script src="{!! asset('/global/vendor/asscrollbar/jquery-asScrollbar.js') !!}"></script>
    <script src="{!! asset('/global/vendor/asscrollable/jquery-asScrollable.js') !!}"></script>
    <script src="{!! asset('/global/vendor/ashoverscroll/jquery-asHoverScroll.js') !!}"></script>
    <script src="{!! asset('/global/vendor/waves/waves.js') !!}"></script>
    
    <!-- Plugins -->
    <script src="{!! asset('/global/vendor/switchery/switchery.js') !!}"></script>
    <script src="{!! asset('/global/vendor/intro-js/intro.js') !!}"></script>
    <script src="{!! asset('/global/vendor/screenfull/screenfull.js') !!}"></script>
    <script src="{!! asset('/global/vendor/slidepanel/jquery-slidePanel.js') !!}"></script>
    <script src="{!! asset('/global/vendor/jquery-placeholder/jquery.placeholder.js') !!}"></script>
    
    <!-- Scripts -->
    <script src="{!! asset('/global/js/Component.js') !!}"></script>
    <script src="{!! asset('/js/Section/GridMenu.js')!!}"></script>
    <script src="{!! asset('/global/js/Plugin.js') !!}"></script>
    <script src="{!! asset('/global/js/Base.js') !!}"></script>
    <script src="{!! asset('/global/js/Config.js') !!}"></script>
    
    <script src="{!! asset('/js/Section/Menubar.js') !!}"></script>
    <script src="{!! asset('/js/Section/Sidebar.js') !!}"></script>
    <script src="{!! asset('/js/Section/PageAside.js') !!}"></script>
    <script src="{!! asset('/js/Plugin/menu.js') !!}"></script>
    
    <!-- Config -->
    <script src="{!! asset('/global/js/config/colors.js') !!}"></script>
    <script src="{!! asset('/js/config/tour.js') !!}"></script>
    
    <!-- Page -->
    <script src="{!! asset('/js/Site.js') !!}"></script>
    <script src="{!! asset('/global/js/Plugin/asscrollable.js') !!}"></script>
    <script src="{!! asset('/global/js/Plugin/slidepanel.js') !!}"></script>
    <script src="{!! asset('/global/js/Plugin/switchery.js') !!}"></script>
    <script src="{!! asset('/global/js/Plugin/jquery-placeholder.js') !!}"></script>
    <script src="{!! asset('/global/js/Plugin/material.js') !!}"></script>

    @yield('js')

    @yield('footer')

</body>
</html>
