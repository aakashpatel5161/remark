@extends('admin.login_master')
@section('title')
Login
@endsection
@section('content')
<!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle">
      <div class="panel">
        <div class="panel-body">
          <div class="brand">
            <img class="brand-img" src="{{url('/')}}/addagram.png" alt="...">
          </div>
          <form method="post" action="#" id="validateForm">
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="email" class="form-control" name="email" id="email"  />
              <label class="floating-label">Email</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="password" class="form-control" name="password" id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[!@#$%^&*()])(?=.*[A-Z]).{8,}" />
              <label class="floating-label">Password</label>
            </div>
            <div class="form-group clearfix">
              <a class="float-right" href="forgot-password.html">Forgot password?</a>
            </div>
            <button type="submit" class="btn btn-primary btn-block btn-lg mt-40">Sign in</button>
          </form>
        </div>
      </div>

      <footer class="page-copyright page-copyright-inverse">
        <p>WEBSITE BY Creation Studio</p>
        <p>© 2018. All RIGHT RESERVED.</p>
        <div class="social">
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-twitter" aria-hidden="true"></i>
        </a>
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-facebook" aria-hidden="true"></i>
        </a>
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-google-plus" aria-hidden="true"></i>
        </a>
        </div>
      </footer>
    </div>
  </div>
  <!-- End Page -->
@endsection
@section('js')
<script type="text/javascript">
  (function(document, window, $){
    'use strict';

    var Site = window.Site;
    $(document).ready(function(){
      Site.run();
    });
  })(document, window, jQuery);
  
  $(document).ready(function(){

      /**
       * Custom validator for contains at least one lower-case letter
       */
      $.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {
          return this.optional(element) || /[a-z]+/.test(value);
      }, "Must have at least one lowercase letter");
       
      /**
       * Custom validator for contains at least one upper-case letter.
       */
      $.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {
          return this.optional(element) || /[A-Z]+/.test(value);
      }, "Must have at least one uppercase letter");
       
      /**
       * Custom validator for contains at least one number.
       */
      $.validator.addMethod("atLeastOneNumber", function (value, element) {
          return this.optional(element) || /[0-9]+/.test(value);
      }, "Must have at least one number");
       
      /**
       * Custom validator for contains at least one symbol.
       */
      $.validator.addMethod("atLeastOneSymbol", function (value, element) {
          return this.optional(element) || /[!@#$%^&*()]+/.test(value);
      }, "Must have at least one symbol");

      $("#validateForm").validate({
        rules: {
          email: {
            required: true,
            email: true
          },
          password: {
            required: true,
            atLeastOneLowercaseLetter: true,
            atLeastOneUppercaseLetter: true,
            atLeastOneNumber: true,
            atLeastOneSymbol: true,
            minlength: 8,
            maxlength: 30
          },
        },
        
        messages: {
          email: "Please enter the email",
          password: {
            required: "Please enter password",
            atLeastOneLowercaseLetter: "There must be one lowercase letter in password",
            atLeastOneUppercaseLetter: "There must be one uppercase letter in password",
            atLeastOneNumber: "There must be one numerical letter in password",
            atLeastOneSymbol: "There must be one symbol letter in password",
            minlength: "Password must be 8 character long",
            maxlength: "Password must be less than 30 character"
          }
        }
       
      });
  });
</script>
@endsection('js')