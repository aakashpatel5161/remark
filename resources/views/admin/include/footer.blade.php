<!-- Footer -->
<footer class="site-footer">
  <div class="site-footer-legal">© 2018 <a href="#">Addagram</a></div>
  <div class="site-footer-right">
    <a href="#" download="test.pdf">Terms &amp; Conditions</a>|
    <a href="#" download="test.pdf">Terms of Use</a>|
    <a href="#" download="test.pdf">Privacy Policy</a>
  </div>
</footer>