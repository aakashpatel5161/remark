@extends('layouts.app') @section('content')
<div class="container no-mrpd">
    <div class="row justify-content-left">
        <div class="col-md-10">
            <div class="card mt-4 ml-2">
                <div class="card-header">{{ __('Create Sponsored Post') }}</div>

                <div class="card-body">
                    @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
                    <form method="POST" action="{{action('SponsoredPostController@store')}}" aria-label="{{ __('Sponsored Post') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="form-group row">
                                <div class="col-md-4 col-form-label text-md-right"></div>

                                <div class="col-md-6">
                                    <input type="file" name="file" id="file" value="" /> @if ($errors->has('file'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>
    
                                    <div class="col-md-6">
                                        <input id="title" type="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}"
                                            required> @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                                <div class="col-md-6">
                                        <textarea id="description" type="text" rows="5" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="message"
                                                value="{{ old('description') }}" required></textarea>
        
                                            @if ($errors->has('description'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                            @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sponsoredid" class="col-md-4 col-form-label text-md-right">{{ __('Sponsored Id') }}</label>

                                <div class="col-md-6">
                                    <input id="sponsoredid" type="sponsoredid" class="form-control{{ $errors->has('sponsoredid') ? ' is-invalid' : '' }}" name="sponsoredid" value="{{ old('sponsoredid') }}"
                                        required> @if ($errors->has('sponsoredid'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sponsoredid') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sponsoredusername" class="col-md-4 col-form-label text-md-right">{{ __('Sponsored Username') }}</label>

                                <div class="col-md-6">
                                    <input id="sponsoredusername" type="sponsoredusername" class="form-control{{ $errors->has('sponsoredusername') ? ' is-invalid' : '' }}" name="sponsoredusername"
                                        value="{{ old('sponsoredusername') }}"> @if ($errors->has('sponsoredusername'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sponsoredusername') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-9">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection