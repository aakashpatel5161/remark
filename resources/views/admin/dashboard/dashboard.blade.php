@extends('admin.master')
@section('title')
Dashboard
@endsection

@section('css')
<link rel="stylesheet" href="{!! asset('/global/vendor/chartist/chartist.css') !!}">
<link rel="stylesheet" href="{!! asset('/global/vendor/jvectormap/jquery-jvectormap.css') !!}">
<link rel="stylesheet" href="{!! asset('/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') !!}">
<link rel="stylesheet" href="{!! asset('/examples/css/dashboard/v1.css') !!}">
<link rel="stylesheet" href="{!! asset('/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css') !!}">
<link rel="stylesheet" href="/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css">
<link rel="stylesheet" href="/examples/css/tables/datatable.css">
@endsection


@section('content')
@include('admin.include.header')
<!-- Page -->
<div class="page">
  <div class="page-content container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
      <div class="col-xl-3 col-md-6">
        <!-- Widget Linearea One-->
        <div class="card card-shadow" id="widgetLineareaOne">
          <div class="card-block p-20 pt-10">
            <div class="clearfix">
              <div class="grey-800 float-left py-10">
                <i class="icon md-account grey-600 font-size-24 vertical-align-bottom mr-5"></i>Total Users
              </div>
              <span class="float-right grey-700 font-size-30">1,253</span>
            </div>
            {{-- <div class="mb-20 grey-500">
              <i class="icon md-long-arrow-up green-500 font-size-16"></i>15% From this yesterday
            </div>
            <div class="ct-chart h-50"></div> --}}
          </div>
        </div>
        <!-- End Widget Linearea One -->
      </div>
      <div class="col-xl-3 col-md-6">
        <!-- Widget Linearea Two -->
        <div class="card card-shadow" id="widgetLineareaTwo">
          <div class="card-block p-20 pt-10">
            <div class="clearfix">
              <div class="grey-800 float-left py-10">
                <i class="icon md-flash grey-600 font-size-24 vertical-align-bottom mr-5"></i>Total Active
              </div>
              <span class="float-right grey-700 font-size-30">2,425</span>
            </div>
            {{-- <div class="mb-20 grey-500">
              <i class="icon md-long-arrow-up green-500 font-size-16"></i>34.2% From this week
            </div>
            <div class="ct-chart h-50"></div> --}}
          </div>
        </div>
        <!-- End Widget Linearea Two -->
      </div>
      <div class="col-xl-3 col-md-6">
        <!-- Widget Linearea Three -->
        <div class="card card-shadow" id="widgetLineareaThree">
          <div class="card-block p-20 pt-10">
            <div class="clearfix">
              <div class="grey-800 float-left py-10">
                <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>Total Posts
              </div>
              <span class="float-right grey-700 font-size-30">1,864</span>
            </div>
            {{-- <div class="mb-20 grey-500">
              <i class="icon md-long-arrow-down red-500 font-size-16"></i>15% From this yesterday
            </div>
            <div class="ct-chart h-50"></div> --}}
          </div>
        </div>
        <!-- End Widget Linearea Three -->
      </div>
      <div class="col-xl-3 col-md-6">
        <!-- Widget Linearea Four -->
        <div class="card card-shadow" id="widgetLineareaFour">
          <div class="card-block p-20 pt-10">
            <div class="clearfix">
              <div class="grey-800 float-left py-10">
                <i class="icon md-view-list grey-600 font-size-24 vertical-align-bottom mr-5"></i>Total Countries
              </div>
              <span class="float-right grey-700 font-size-30">845</span>
            </div>
            {{-- <div class="mb-20 grey-500">
              <i class="icon md-long-arrow-up green-500 font-size-16"></i>18.4% From this yesterday
            </div>
            <div class="ct-chart h-50"></div> --}}
          </div>
        </div>
        <!-- End Widget Linearea Four -->
      </div>

      {{-- <div class="col-xxl-7 col-lg-9">
        <!-- Widget Jvmap -->
        
        <!-- End Widget Jvmap -->
      </div> --}}


      {{-- <div class="col-xxl-5 col-lg-5">
        <!-- Widget Current Chart -->
        <div class="card card-shadow" id="widgetCurrentChart">
          <div class="p-30 white bg-green-500">
            <div class="font-size-20 mb-20">The current chart</div>
            <div class="ct-chart h-200">
            </div>
          </div>
          <div class="bg-white p-30 font-size-14">
            <div class="counter counter-lg text-left">
              <div class="counter-label mb-5">Approve rate are above average</div>
              <div class="counter-number-group">
                <span class="counter-number">12,673</span>
                <span class="counter-number-related text-uppercase font-size-14">pcs</span>
              </div>
            </div>
            <button type="button" class="btn-raised btn btn-info btn-floating">
              <i class="icon md-plus" aria-hidden="true"></i>
            </button>
          </div>
        </div>
        <!-- End Widget Current Chart -->
      </div> --}}

      {{-- <div class="col-xl-4 col-lg-6">
        <!-- Widget User list -->
        <div class="card" id="widgetUserList">
          <div class="card-header cover overlay">
            <img class="cover-image h-200" src="{{url('/')}}//examples/images/dashboard-header.jpg"
              alt="..." />
            <div class="overlay-panel vertical-align overlay-background">
              <div class="vertical-align-middle">
                <a class="avatar avatar-100 float-left mr-20" href="javascript:void(0)">
                  <img src="{{url('/')}}/global/portraits/5.jpg" alt="">
                </a>
                <div class="float-left">
                  <div class="font-size-20">Robin Ahrens</div>
                  <p class="mb-20 text-nowrap">
                    <span class="text-break">machidesign@gmail</span>
                  </p>
                  <div class="text-nowrap font-size-18">
                    <a href="" class="white mr-10">
                  <i class="icon bd-twitter"></i>
                </a>
                    <a href="" class="white mr-10">
                  <i class="icon bd-facebook"></i>
                </a>
                    <a href="" class="white">
                  <i class="icon bd-dribbble"></i>
                </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-block py-0">
            <ul class="list-group list-group-full list-group-dividered mb-0">
              <li class="list-group-item">
                <div class="media">
                  <div class="pr-20">
                    <a class="avatar avatar-lg" href="javascript:void(0)">
                      <img class="img-responsive" src="{{url('/')}}/global/portraits/1.jpg"
                        alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <h5 class="mt-0 mb-5">Herman Beck</h5>
                    <small>San Francisco</small>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="pr-20">
                    <a class="avatar avatar-lg" href="javascript:void(0)">
                      <img class="img-responsive" src="{{url('/')}}/global/portraits/2.jpg"
                        alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <h5 class="mt-0 mb-5">Mary Adams</h5>
                    <small>Salt Lake City, Utah</small>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="pr-20">
                    <a class="avatar avatar-lg" href="javascript:void(0)">
                      <img class="img-responsive" src="{{url('/')}}/global/portraits/3.jpg"
                        alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <h5 class="mt-0 mb-5">Caleb Richards</h5>
                    <small>Basking Ridge, NJ</small>
                  </div>
                </div>
              </li>
            </ul>
            <button type="button" class="btn-raised btn btn-danger btn-floating">
              <i class="icon md-plus" aria-hidden="true"></i>
            </button>
          </div>
        </div>
        <!-- End Widget User list -->
      </div>

      <div class="col-xl-4 col-lg-6">
        <!-- Widget Chat -->
        <div class="card card-shadow" id="chat">
          <div class="card-header bg-white px-0">
            <a class="float-left" href="javascript:void(0)">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
        </a>
            <div class="text-right">
              Conversation with
              <span class="hidden-xs-down">June Lane</span>
              <a class="avatar ml-10" data-toggle="tooltip" href="#" data-placement="right" title="June Lane">
                <img src="{{url('/')}}/global/portraits/4.jpg" alt="...">
              </a>
            </div>
          </div>
          <div class="card-block">
            <div class="chats">
              <div class="chat chat-left">
                <div class="chat-avatar">
                  <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="">
                    <img src="{{url('/')}}/global/portraits/5.jpg" alt="Edward Fletcher">
                  </a>
                </div>
                <div class="chat-body">
                  <div class="chat-content" data-toggle="tooltip" title="11:37:08 am">
                    <p>Good morning, sir.</p>
                    <p>What can I do for you?</p>
                  </div>
                </div>
              </div>
              <div class="chat">
                <div class="chat-avatar">
                  <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="">
                    <img src="{{url('/')}}/global/portraits/4.jpg" alt="June Lane">
                  </a>
                </div>
                <div class="chat-body">
                  <div class="chat-content" data-toggle="tooltip" title="11:39:57 am">
                    <p>Well, I am just looking around.</p>
                  </div>
                </div>
              </div>
              <div class="chat chat-left">
                <div class="chat-avatar">
                  <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="">
                    <img src="{{url('/')}}/global/portraits/5.jpg" alt="Edward Fletcher">
                  </a>
                </div>
                <div class="chat-body">
                  <div class="chat-content" data-toggle="tooltip" title="11:40:10 am">
                    <p>If necessary, please ask me.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer bg-white">
            <form>
              <div class="input-group form-material">
                <span class="input-group-btn">
                  <a href="javascript: void(0)" class="btn btn-pure btn-default icon md-camera"></a>
                </span>
                <input class="form-control" type="text" placeholder="Type message here ...">
                <span class="input-group-btn">
                  <button class="btn btn-pure btn-default icon md-mail-send" type="button"></button>
                </span>
              </div>
            </form>
          </div>
        </div>
        <!-- End Widget Chat -->
      </div>

      <div class="col-xl-4 col-lg-6">
        <!-- Widget Info -->
        <div class="card card-shadow">
          <div class="card-header cover overlay">
            <div class="cover-background h-200" style="background-image: url('{{url('/')}}/global/photos/placeholder.png')"></div>
          </div>
          <div class="card-block px-30 py-20" style="height:calc(100% - 250px);">
            <div class="mb-10" style="margin-top: -70px;">
              <a class="avatar avatar-100 bg-white img-bordered" href="javascript:void(0)">
                <img src="{{url('/')}}/global/portraits/2.jpg" alt="">
              </a>
            </div>
            <div class="mb-20">
              <div class="font-size-20">Caleb Richards</div>
              <div class="font-size-14 grey-500">
                <span>2 hours ago</span> |
                <span>Comments 20</span>
              </div>
            </div>
            <p>
              Lorem ipsum dolLorem ip sum dolor sit amet, consectetur adipiscing elit. Integer
              nec odio. Praesent libero.or sit amet, consectetur adipiscing
              elit. Integer nec odio. Praesent libero.
            </p>
          </div>
        </div>
        <!-- End Widget Info -->
      </div>

      <div class="col-xxl-5 col-lg-6">
        <!-- Panel Projects -->
        <div class="panel" id="projects">
          <div class="panel-heading">
            <h3 class="panel-title">Projects</h3>
          </div>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <td>Projects</td>
                  <td>Completed</td>
                  <td>Date</td>
                  <td>Actions</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>The sun climbing plan</td>
                  <td>
                    <span data-plugin="peityPie" data-skin="red">7/10</span>
                  </td>
                  <td>Jan 1, 2017</td>
                  <td>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Edit">
                      <i class="icon md-wrench" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Delete">
                      <i class="icon md-close" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
                <tr>
                  <td>Lunar probe project</td>
                  <td>
                    <span data-plugin="peityPie" data-skin="blue">3/10</span>
                  </td>
                  <td>Feb 12, 2017</td>
                  <td>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Edit">
                      <i class="icon md-wrench" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Delete">
                      <i class="icon md-close" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
                <tr>
                  <td>Dream successful plan</td>
                  <td>
                    <span data-plugin="peityPie" data-skin="green">9/10</span>
                  </td>
                  <td>Apr 9, 2017</td>
                  <td>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Edit">
                      <i class="icon md-wrench" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Delete">
                      <i class="icon md-close" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
                <tr>
                  <td>Office automatization</td>
                  <td>
                    <span data-plugin="peityPie" data-skin="orange">5/10</span>
                  </td>
                  <td>May 15, 2017</td>
                  <td>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Edit">
                      <i class="icon md-wrench" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Delete">
                      <i class="icon md-close" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
                <tr>
                  <td>Open strategy</td>
                  <td>
                    <span data-plugin="peityPie" data-skin="brown">2/10</span>
                  </td>
                  <td>Jun 2, 2017</td>
                  <td>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Edit">
                      <i class="icon md-wrench" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-default"
                      data-toggle="tooltip" data-original-title="Delete">
                      <i class="icon md-close" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- End Panel Projects -->
      </div>

      <div class="col-xxl-7 col-lg-6">
        <!-- Panel Projects Status -->
        <div class="panel" id="projects-status">
          <div class="panel-heading">
            <h3 class="panel-title">
              Projects Status
              <span class="badge badge-pill badge-info">5</span>
            </h3>
          </div>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <td>ID</td>
                  <td>Project</td>
                  <td>Status</td>
                  <td class="text-left">Progress</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>619</td>
                  <td>The sun climbing plan</td>
                  <td>
                    <span class="badge badge-primary">Developing</span>
                  </td>
                  <td>
                    <span data-plugin="peityLine">5,3,2,-1,-3,-2,2,3,5,2</span>
                  </td>
                </tr>
                <tr>
                  <td>620</td>
                  <td>Lunar probe project</td>
                  <td>
                    <span class="badge badge-warning">Design</span>
                  </td>
                  <td>
                    <span data-plugin="peityLine">1,-1,0,2,3,1,-1,1,4,2</span>
                  </td>
                </tr>
                <tr>
                  <td>621</td>
                  <td>Dream successful plan</td>
                  <td>
                    <span class="badge badge-info">Testing</span>
                  </td>
                  <td>
                    <span data-plugin="peityLine">2,3,-1,-3,-1,0,2,4,5,3</span>
                  </td>
                </tr>
                <tr>
                  <td>622</td>
                  <td>Office automatization</td>
                  <td>
                    <span class="badge badge-danger">Canceled</span>
                  </td>
                  <td>
                    <span data-plugin="peityLine">1,-2,0,2,4,5,3,2,4,2</span>
                  </td>
                </tr>
                <tr>
                  <td>623</td>
                  <td>Open strategy</td>
                  <td>
                    <span class="badge badge-default">Reply waiting</span>
                  </td>
                  <td>
                    <span data-plugin="peityLine">4,2,-1,-3,-2,1,3,5,2,4</span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- End Panel Projects Stats -->
      </div> --}}
    </div>
        <!-- Panel Basic -->
        <div class="panel panel-primary panel-line">
            <header class="panel-heading">
              <div class="panel-actions"></div>
              <h3 class="panel-title">Registered Users in Last 24 Hours</h3>
            </header>
            <div class="panel-body">
              <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Salary</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <tr>
                    <td>Damon</td>
                    <td>5516 Adolfo Green</td>
                    <td>Littelhaven</td>
                    <td>85</td>
                    <td>2014/06/13</td>
                    <td>$553,536</td>
                  </tr>
                  <tr>
                    <td>Torrey</td>
                    <td>1995 Richie Neck</td>
                    <td>West Sedrickstad</td>
                    <td>77</td>
                    <td>2014/09/12</td>
                    <td>$243,577</td>
                  </tr>
                  <tr>
                    <td>Miracle</td>
                    <td>176 Hirthe Squares</td>
                    <td>Ryleetown</td>
                    <td>82</td>
                    <td>2013/09/27</td>
                    <td>$784,802</td>
                  </tr>
                  <tr>
                    <td>Wilhelmine</td>
                    <td>44727 O&#x27;Hara Union</td>
                    <td>Dibbertfurt</td>
                    <td>68</td>
                    <td>2013/06/28</td>
                    <td>$207,291</td>
                  </tr>
                  <tr>
                    <td>Hubert</td>
                    <td>8884 Jamel Pines</td>
                    <td>Howemouth</td>
                    <td>63</td>
                    <td>2013/05/28</td>
                    <td>$584,032</td>
                  </tr>
                  <tr>
                    <td>Myrtie.Gerhold</td>
                    <td>098 Noel Way</td>
                    <td>Santinoland</td>
                    <td>13</td>
                    <td>2014/12/12</td>
                    <td>$550,087</td>
                  </tr>
                  <tr>
                    <td>Chester</td>
                    <td>14095 Kling Gateway</td>
                    <td>Andresmouth</td>
                    <td>26</td>
                    <td>2014/09/27</td>
                    <td>$177,404</td>
                  </tr>
                  <tr>
                    <td>Melany_Gerhold</td>
                    <td>1100 Steve Pines</td>
                    <td>Immanuelfort</td>
                    <td>12</td>
                    <td>2014/06/28</td>
                    <td>$162,453</td>
                  </tr>
                  <tr>
                    <td>Thea</td>
                    <td>26114 Narciso Lodge</td>
                    <td>East Opal</td>
                    <td>64</td>
                    <td>2014/11/12</td>
                    <td>$581,736</td>
                  </tr>
                  <tr>
                    <td>Albin.Kreiger</td>
                    <td>111 Hershel Stream</td>
                    <td>Hermannborough</td>
                    <td>90</td>
                    <td>2013/11/27</td>
                    <td>$921,021</td>
                  </tr>
                  <tr>
                    <td>Shanel</td>
                    <td>7579 Santa Forest</td>
                    <td>Jordaneville</td>
                    <td>14</td>
                    <td>2017/04/28</td>
                    <td>$818,20</td>
                  </tr>
                  <tr>
                    <td>Bell.Mueller</td>
                    <td>083 Kshlerin Forest</td>
                    <td>Clintmouth</td>
                    <td>98</td>
                    <td>2013/10/12</td>
                    <td>$571,46</td>
                  </tr>
                  <tr>
                    <td>Clementina</td>
                    <td>5957 Hagenes Falls</td>
                    <td>Anaishaven</td>
                    <td>45</td>
                    <td>2013/11/12</td>
                    <td>$684,588</td>
                  </tr>
                  <tr>
                    <td>Johanna.Thiel</td>
                    <td>4301 Trever Radial</td>
                    <td>Lake Augustineton</td>
                    <td>67</td>
                    <td>2013/12/27</td>
                    <td>$608,507</td>
                  </tr>
                  <tr>
                    <td>Elliott_Becker</td>
                    <td>8417 Orion Parkway</td>
                    <td>Streichside</td>
                    <td>83</td>
                    <td>2014/08/28</td>
                    <td>$447,592</td>
                  </tr>
                  <tr>
                    <td>Yasmine</td>
                    <td>67284 Kreiger Freeway</td>
                    <td>Stoltenbergside</td>
                    <td>8</td>
                    <td>2014/12/12</td>
                    <td>$358,369</td>
                  </tr>
                  <tr>
                    <td>Ada.Hoppe</td>
                    <td>69842 Peyton Viaduct</td>
                    <td>South Geovannyburgh</td>
                    <td>89</td>
                    <td>2013/05/13</td>
                    <td>$211,76</td>
                  </tr>
                  <tr>
                    <td>Sammie</td>
                    <td>46406 Powlowski Common</td>
                    <td>Cristmouth</td>
                    <td>51</td>
                    <td>2014/03/29</td>
                    <td>$345,739</td>
                  </tr>
                  <tr>
                    <td>Terrance.Borer</td>
                    <td>1568 Richmond Bypass</td>
                    <td>Schillerfort</td>
                    <td>46</td>
                    <td>2014/10/27</td>
                    <td>$634,073</td>
                  </tr>
                  <tr>
                    <td>August</td>
                    <td>731 Stiedemann Crossing</td>
                    <td>Rolfsonborough</td>
                    <td>98</td>
                    <td>2013/11/12</td>
                    <td>$203,952</td>
                  </tr>
                  <tr>
                    <td>Mckenna.Herman</td>
                    <td>63204 Hegmann Keys</td>
                    <td>New Isobelview</td>
                    <td>2</td>
                    <td>2013/08/12</td>
                    <td>$702,091</td>
                  </tr>
                  <tr>
                    <td>Adrianna_Durgan</td>
                    <td>75151 Kshlerin Square</td>
                    <td>North Elwynfurt</td>
                    <td>25</td>
                    <td>2014/02/26</td>
                    <td>$481,980</td>
                  </tr>
                  <tr>
                    <td>Heath.Ryan</td>
                    <td>6778 Howe Route</td>
                    <td>Antwanbury</td>
                    <td>90</td>
                    <td>2013/08/12</td>
                    <td>$569,723</td>
                  </tr>
                  <tr>
                    <td>Alisa</td>
                    <td>64838 D&#x27;Amore Cove</td>
                    <td>Port Lempi</td>
                    <td>25</td>
                    <td>2017/04/28</td>
                    <td>$226,459</td>
                  </tr>
                  <tr>
                    <td>Treva</td>
                    <td>308 Octavia Roads</td>
                    <td>East Eunaton</td>
                    <td>37</td>
                    <td>2014/12/12</td>
                    <td>$746,921</td>
                  </tr>
                  <tr>
                    <td>Myriam_Nicolas</td>
                    <td>760 Hickle Causeway</td>
                    <td>Lake Nickolasshire</td>
                    <td>69</td>
                    <td>2014/05/13</td>
                    <td>$293,786</td>
                  </tr>
                  <tr>
                    <td>Gerhard</td>
                    <td>893 Mara Parkway</td>
                    <td>Elmermouth</td>
                    <td>32</td>
                    <td>2014/11/27</td>
                    <td>$856,275</td>
                  </tr>
                  <tr>
                    <td>Monica</td>
                    <td>0039 Heath Plain</td>
                    <td>West Bentonhaven</td>
                    <td>80</td>
                    <td>2017/05/13</td>
                    <td>$821,600</td>
                  </tr>
                  <tr>
                    <td>Lacey</td>
                    <td>995 Lang Springs</td>
                    <td>Cordellburgh</td>
                    <td>94</td>
                    <td>2014/11/27</td>
                    <td>$990,291</td>
                  </tr>
                  <tr>
                    <td>Bret</td>
                    <td>282 Susana Heights</td>
                    <td>Kaneport</td>
                    <td>47</td>
                    <td>2013/05/28</td>
                    <td>$445,494</td>
                  </tr>
                  <tr>
                    <td>Jennie</td>
                    <td>755 Greyson Key</td>
                    <td>East Jazmyne</td>
                    <td>94</td>
                    <td>2017/03/29</td>
                    <td>$530,408</td>
                  </tr>
                  <tr>
                    <td>Emerson</td>
                    <td>784 Adriel Radial</td>
                    <td>New Jerroldland</td>
                    <td>4</td>
                    <td>2014/02/26</td>
                    <td>$805,823</td>
                  </tr>
                  <tr>
                    <td>Herta</td>
                    <td>7491 Bednar Gardens</td>
                    <td>Port Lucianoton</td>
                    <td>23</td>
                    <td>2013/10/12</td>
                    <td>$352,269</td>
                  </tr>
                  <tr>
                    <td>Stone_Deckow</td>
                    <td>6440 Moen Loop</td>
                    <td>Jenningsbury</td>
                    <td>23</td>
                    <td>2014/07/28</td>
                    <td>$219,573</td>
                  </tr>
                  <tr>
                    <td>Davin</td>
                    <td>50922 Kiara Square</td>
                    <td>Port Edmund</td>
                    <td>37</td>
                    <td>2014/11/27</td>
                    <td>$241,432</td>
                  </tr>
                  <tr>
                    <td>Johnathan_Mraz</td>
                    <td>1998 Webster Fords</td>
                    <td>East Vern</td>
                    <td>50</td>
                    <td>2014/09/12</td>
                    <td>$290,875</td>
                  </tr>
                  <tr>
                    <td>Gunnar</td>
                    <td>92873 Barney Club</td>
                    <td>Beierview</td>
                    <td>82</td>
                    <td>2014/03/29</td>
                    <td>$569,778</td>
                  </tr>
                  <tr>
                    <td>Raina</td>
                    <td>828 Cathy Streets</td>
                    <td>West Uriahville</td>
                    <td>26</td>
                    <td>2013/09/27</td>
                    <td>$186,229</td>
                  </tr>
                  <tr>
                    <td>Marjorie.Orn</td>
                    <td>314 Aurore Canyon</td>
                    <td>Port Jaquelineburgh</td>
                    <td>3</td>
                    <td>2014/06/28</td>
                    <td>$547,752</td>
                  </tr>
                  <tr>
                    <td>Citlalli_Wehner</td>
                    <td>139 Ebert Freeway</td>
                    <td>Lake Esperanzamouth</td>
                    <td>78</td>
                    <td>2017/01/27</td>
                    <td>$892,012</td>
                  </tr>
                  <tr>
                    <td>Ruben.Reilly</td>
                    <td>02868 Cronin Tunnel</td>
                    <td>Rossieville</td>
                    <td>87</td>
                    <td>2013/09/12</td>
                    <td>$520,483</td>
                  </tr>
                  <tr>
                    <td>Gunner_Jakubowski</td>
                    <td>80391 Maxwell Parks</td>
                    <td>South Travon</td>
                    <td>26</td>
                    <td>2014/03/29</td>
                    <td>$272,005</td>
                  </tr>
                  <tr>
                    <td>Josephine</td>
                    <td>36238 Satterfield Avenue</td>
                    <td>New Alexanderhaven</td>
                    <td>51</td>
                    <td>2017/01/27</td>
                    <td>$189,18</td>
                  </tr>
                  <tr>
                    <td>Ceasar_Orn</td>
                    <td>2795 Clement Ridges</td>
                    <td>Beckerhaven</td>
                    <td>78</td>
                    <td>2013/11/27</td>
                    <td>$958,117</td>
                  </tr>
                  <tr>
                    <td>Coby</td>
                    <td>53700 Pagac Lodge</td>
                    <td>South Hershel</td>
                    <td>86</td>
                    <td>2013/08/28</td>
                    <td>$481,619</td>
                  </tr>
                  <tr>
                    <td>Colin</td>
                    <td>637 Paucek Mountain</td>
                    <td>West Luraberg</td>
                    <td>77</td>
                    <td>2017/02/26</td>
                    <td>$298,110</td>
                  </tr>
                  <tr>
                    <td>Monique_White</td>
                    <td>415 Corkery Walks</td>
                    <td>West Lauryn</td>
                    <td>97</td>
                    <td>2014/02/11</td>
                    <td>$222,343</td>
                  </tr>
                  <tr>
                    <td>Jarvis.Simonis</td>
                    <td>0778 Elvis Spurs</td>
                    <td>Harrisfurt</td>
                    <td>62</td>
                    <td>2013/05/28</td>
                    <td>$336,046</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
  </div>
</div>
<!-- End Page -->


  </div>
@endsection

@section('js')
<script src="{!! asset('/global/vendor/chartist/chartist.min.js') !!}"></script>
<script src="{!! asset('/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js') !!}"></script>
<script src="{!! asset('/global/vendor/jvectormap/jquery-jvectormap.min.js') !!}"></script>
<script src="{!! asset('/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js') !!}"></script>
<script src="{!! asset('/global/vendor/matchheight/jquery.matchHeight-min.js') !!}"></script>
<script src="{!! asset('/global/vendor/peity/jquery.peity.min.js') !!}"></script>
<script src="/global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
<script src="/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
<script src="/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
<script src="/global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
<script src="/global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="/global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.html5.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.flash.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.print.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
<script src="/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
<script src="/global/vendor/asrange/jquery-asRange.min.js"></script>
<script src="/global/vendor/bootbox/bootbox.js"></script>
<script src="/global/js/Plugin/datatables.js"></script>
<script src="/examples/js/tables/datatable.js"></script>
<script src="/examples/js/uikit/icon.js"></script>
@endsection
    
@section('footer')
@include('admin.include.footer')
@endsection