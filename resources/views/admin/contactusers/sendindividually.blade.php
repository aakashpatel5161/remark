@extends('admin.master')
@section('title')
Contact Individually
@endsection

@section('css')
<link rel="stylesheet" href="{!! asset('/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css') !!}">
<link rel="stylesheet" href="/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css">
<link rel="stylesheet" href="/examples/css/tables/datatable.css">
@endsection


@section('content')
@include('admin.include.header')

<div class="page">
        <div class="page-content">
                <div class="col-xxl-12 col-lg-12">
                      <!-- Panel Basic -->
        <div class="panel panel-primary panel-line">
            <header class="panel-heading">
              <div class="panel-actions">
                  <button type="button" class="btn btn-icon btn-primary" data-target="#selectedusersmodal" data-toggle="modal">Send to Selected Users</button>                       
              </div>
              <h3 class="panel-title">Contact Individually</h3>
            </header>
            <div class="panel-body">
              <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Country</th>
                    <th>Registered</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <td>
                      <input type="checkbox" id="inputCheckbox" class="checkbox-custom checkbox-default" name="inputCheckbox">                                
                     </td>
                      <td>John</td>
                    <td>5516 Adolfo Green</td>
                    <td>Littelhaven</td>
                    <td>85</td>                    
                    <td><div class="row ml-3">  
                        <button type="button" class="btn btn-icon btn-primary" data-target="#sendmodal" data-toggle="modal"><i class="icon md-email" aria-hidden="true"></i></button>&nbsp;                        
                      </div></td>
                  </tr>
                  <tr>
                      <td>
                          <input type="checkbox" id="inputCheckbox" class="checkbox-custom checkbox-default" name="inputCheckbox" >                                
                      </td>
                    <td>Torrey</td>
                    <td>1995 Richie Neck</td>
                    <td>West Sedrickstad</td>
                    <td>77</td>
                    <td>2014/09/12</td>                    
                  </tr>
                  <tr>
                      <td>
                          <input type="checkbox" id="inputCheckbox" class="checkbox-custom checkbox-default" name="inputCheckbox" >                                
                         </td>
                    <td>Miracle</td>
                    <td>176 Hirthe Squares</td>
                    <td>Ryleetown</td>
                    <td>82</td>
                    <td>2013/09/27</td>                    
                  </tr>
                  <tr>
                      <td>
                          <input type="checkbox" id="inputCheckbox" class="checkbox-custom checkbox-default" name="inputCheckbox" >                                
                         </td>                    
                    <td>Wilhelmine</td>
                    <td>44727 O&#x27;Hara Union</td>
                    <td>Dibbertfurt</td>
                    <td>68</td>
                    <td>2013/06/28</td>                  
                  </tr>
                  <tr>
                      <td>
                          <input type="checkbox" id="inputCheckbox" class="checkbox-custom checkbox-default" name="inputCheckbox" >                                
                         </td>
                    <td>Hubert</td>
                    <td>8884 Jamel Pines</td>
                    <td>Howemouth</td>
                    <td>63</td>
                    <td>2013/05/28</td>                    
                  </tr>            
                </tbody>
              </table>
            </div>
          </div>
                        <!-- End Example Heading With Desc -->
                      </div>
        </div>
</div>
<!-- Contact Modal -->
<div class="modal fade modal-3d-flip-horizontal" id="sendmodal"
aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
tabindex="-1">
<div class="modal-dialog modal-simple">
    <form class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="exampleFormModalLabel">Contact User</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-xl-12 form-group">
                  <label for="name">Name</label>
                  <input type="email" class="form-control" name="name" placeholder="Name" disabled>
                </div>
                <div class="col-xl-12 form-group">
                    <label for="email">Email</label>
                  <input type="email" class="form-control" name="email" placeholder="Email" disabled>
                </div>
                <div class="col-xl-12 form-group">
                    <label for="username">Username</label>
                    <input type="email" class="form-control" name="username" placeholder="Username" disabled>
                </div>
                <div class="col-xl-12 form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" rows="5" placeholder="Type your message"></textarea>
                  </div>
              </div>
        </div>
        <div class="modal-footer">    
            <button type="button" class="btn btn-primary">Send</button>
          </div>
      </form>
</div>
</div>
<!-- End Modal -->

<!-- Contact Modal -->
<div class="modal fade modal-3d-flip-horizontal" id="selectedusersmodal"
aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
tabindex="-1">
<div class="modal-dialog modal-simple">
    <form class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="exampleFormModalLabel">Contact Selected User</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-xl-12 form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" rows="5" placeholder="Type your message"></textarea>
                  </div>
              </div>
        </div>
        <div class="modal-footer">    
            <button type="button" class="btn btn-primary">Send</button>
          </div>
      </form>
</div>
</div>
<!-- End Modal -->

@endsection

@section('js')
<script src="/global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
<script src="/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
<script src="/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
<script src="/global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
<script src="/global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="/global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.html5.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.flash.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.print.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
<script src="/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
<script src="/global/vendor/asrange/jquery-asRange.min.js"></script>
<script src="/global/vendor/bootbox/bootbox.js"></script>
<script src="/global/js/Plugin/datatables.js"></script>
<script src="/examples/js/tables/datatable.js"></script>
<script src="/examples/js/uikit/icon.js"></script>>
<script src="/global/js/Plugin/material.js"></script>
<script src="/examples/js/forms/advanced.js"></script>
@endsection
    
@section('footer')
@include('admin.include.footer')
@endsection