@extends('admin.master')
@section('title')
Contact By Country
@endsection

@section('css')

@endsection


@section('content')
@include('admin.include.header')

<div class="page">
        <div class="page-content">
                <div class="col-xxl-6 col-lg-8">
                        <!-- Example Heading With Desc -->
                        <div class="panel panel-primary panel-line">
                          <div class="panel-heading">
                            <h3 class="panel-title">Contact User by Country
                            </h3>
                          </div>
                          <div class="panel-body">
                                <form autocomplete="off">
                                        <div class="form-group form-material" data-plugin="formMaterial">
                                                <label class="form-control-label" for="select">Select</label>
                                                <select class="form-control" id="country" name="country" required>
                                                  <option>1</option>
                                                  <option>2</option>
                                                  <option>3</option>
                                                  <option>4</option>
                                                  <option>5</option>
                                                </select>
                                              </div>
                                        <div class="form-group form-material" data-plugin="formMaterial">
                                          <label class="form-control-label" for="textarea">Message</label>
                                          <textarea class="form-control" id="textarea" name="message" rows="3"></textarea>
                                        </div>
                                        <div class="form-group form-material" data-plugin="formMaterial">
                                            <button type="button" class="btn btn-primary">Send Message</button>
                                        </div>
                                </form>
                          </div>
                        </div>
                        <!-- End Example Heading With Desc -->
                      </div>
        </div>
</div>


@endsection

@section('js')

<script src="/global/vendor/asprogress/jquery-asProgress.js"></script>
<script src="/global/vendor/draggabilly/draggabilly.pkgd.js"></script>
<script src="/global/vendor/raty/jquery.raty.js"></script>
<script src="/global/js/Plugin/responsive-tabs.js"></script>
<script src="/global/js/Plugin/tabs.js"></script>
<script src="/global/js/Plugin/asprogress.js"></script>
<script src="/global/js/Plugin/panel.js"></script>
<script src="/global/js/Plugin/asscrollable.js"></script>
<script src="/global/js/Plugin/raty.js"></script>
<script src="/examples/js/uikit/panel-structure.js"></script>
<script src="/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="/global/js/Plugin/jquery-placeholder.js"></script>
<script src="/global/js/Plugin/material.js"></script>
@endsection
    
@section('footer')
@include('admin.include.footer')
@endsection