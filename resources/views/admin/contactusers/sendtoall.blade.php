@extends('admin.master')
@section('title')
Send All
@endsection

@section('css')

@endsection


@section('content')
@include('admin.include.header')

<div class="page">
        <div class="page-content">
                <div class="col-xxl-6 col-lg-8">
                        <!-- Example Heading With Desc -->
                        <div class="panel panel-primary panel-line">
                          <div class="panel-heading">
                            <h3 class="panel-title">Contact All
                            </h3>
                          </div>
                          <div class="panel-body">
                                <form autocomplete="off">

                                        <div class="form-group form-material" data-plugin="formMaterial">
                                          <label class="form-control-label" for="textarea">Message</label>
                                          <textarea class="form-control" id="textarea" name="message" rows="3"></textarea>
                                        </div>
                                        <div class="form-group form-material">
                                                <button type="button" class="btn btn-primary">Send Message</button>
                                        </div>
                                </form>
                          </div>
                        </div>
                        <!-- End Example Heading With Desc -->
                      </div>
                
        </div>
</div>


@endsection

@section('js')
{{-- <script src="{!! asset('/global/vendor/chartist/chartist.min.js') !!}"></script>
<script src="{!! asset('/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js') !!}"></script>
<script src="{!! asset('/global/vendor/jvectormap/jquery-jvectormap.min.js') !!}"></script>
<script src="{!! asset('/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js') !!}"></script>
<script src="{!! asset('/global/vendor/matchheight/jquery.matchHeight-min.js') !!}"></script>
<script src="{!! asset('/global/vendor/peity/jquery.peity.min.js') !!}"></script>
<script src="/global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
<script src="/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
<script src="/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
<script src="/global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
<script src="/global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="/global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.html5.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.flash.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.print.js"></script>
<script src="/global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
<script src="/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
<script src="/global/vendor/asrange/jquery-asRange.min.js"></script>
<script src="/global/vendor/bootbox/bootbox.js"></script>
<script src="/global/js/Plugin/datatables.js"></script>
<script src="/examples/js/tables/datatable.js"></script>
<script src="/examples/js/uikit/icon.js"></script> --}}
<script src="/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="/global/js/Plugin/jquery-placeholder.js"></script>
<script src="/global/js/Plugin/material.js"></script>
<script src="/global/vendor/asprogress/jquery-asProgress.js"></script>
<script src="/global/vendor/draggabilly/draggabilly.pkgd.js"></script>
<script src="/global/vendor/raty/jquery.raty.js"></script>
<script src="/global/js/Plugin/responsive-tabs.js"></script>
<script src="/global/js/Plugin/tabs.js"></script>
<script src="/global/js/Plugin/asprogress.js"></script>
<script src="/global/js/Plugin/panel.js"></script>
<script src="/global/js/Plugin/asscrollable.js"></script>
<script src="/global/js/Plugin/raty.js"></script>
<script src="/examples/js/uikit/panel-structure.js"></script>
@endsection
    
@section('footer')
@include('admin.include.footer')
@endsection