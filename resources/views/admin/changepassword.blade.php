@extends('admin.master')
@section('title')
Change Password
@endsection

@section('css')
<link rel="stylesheet" href="/examples/css/forms/layouts.css">
@endsection


@section('content')
@include('admin.include.header')

<div class="page">
    <div class="page-content">
        <div class="col-xxl-8 col-lg-10">
            <div class="panel panel-primary panel-line">
                    <div class="panel-heading">
                        <h3 class="panel-title">Change Password
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="example-wrap">                            
                                <div class="example">
                                    <form autocomplete="off">
                                           
                                    <div class="form-group form-material">
                                            <label class="form-control-label" for="inputBasicEmail">Current Password</label>
                                            <input type="email" class="form-control" id="name" name="oldpassword"
                                             autocomplete="off" />
                                    </div>
                                    <div class="form-group form-material">
                                            <label class="form-control-label" for="inputBasicEmail">New Password</label>
                                            <input type="email" class="form-control" id="name" name="newpassword"
                                             autocomplete="off" />
                                    </div>
                                    <div class="form-group form-material">
                                            <label class="form-control-label" for="inputBasicEmail">Confirm Password</label>
                                            <input type="email" class="form-control" id="name" name=" newpassword_confirmation"
                                             autocomplete="off" />
                                    </div>
                                    
                                    <div class="form-group form-material">
                                        <button type="button" class="btn btn-primary">Update Password</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                                    <!-- End Example Basic Form -->
                                </div>
                    </div>
                </div>
                              <!-- End Example Heading With Desc -->
        </div>
</div>


@endsection

@section('js')
<script src="/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="/global/js/Plugin/jquery-placeholder.js"></script>
<script src="/global/js/Plugin/material.js"></script>
<script src="/global/vendor/asprogress/jquery-asProgress.js"></script>
<script src="/global/vendor/draggabilly/draggabilly.pkgd.js"></script>
<script src="/global/vendor/raty/jquery.raty.js"></script>
<script src="/global/js/Plugin/responsive-tabs.js"></script>
<script src="/global/js/Plugin/tabs.js"></script>
<script src="/global/js/Plugin/asprogress.js"></script>
<script src="/global/js/Plugin/panel.js"></script>
<script src="/global/js/Plugin/asscrollable.js"></script>
<script src="/global/js/Plugin/raty.js"></script>
<script src="/examples/js/uikit/panel-structure.js"></script>
<script src="/global/js/Plugin/input-group-file.js"></script>
@endsection
    
@section('footer')
@include('admin.include.footer')
@endsection