<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

// model import
use App\Admin;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.admin.admin_view');
    }
    public function changestatus(Request $request,Admin $admin){
        if($request->ajax()) {
            $admin = $admin->find($request->input('data'));
            $admin->status = ($admin->status == "Active") ? 'Inactive' : 'Active';
            if ($admin->save()) {
                return 1;
            }
        }else{
        	return 0;
        }
    }
    public function create(){
        return view('admin.admin.admin_create');
    }
    public function store(Request $request,Admin $admin)
    {
    	$this->validate(request(),[
    		'first_name' => 'required',
    		'last_name' => 'required',
     		 'email' => 'required|email',
            'password' => 'required'
     	]);
        $data = array(
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password)
        );
        $admin = $admin->create($data);
        if($admin->id>0){
            Session::flash('success_msg', 'Admin added successfully!');
        }else{
            Session::flash('error_msg', 'Failed to add admin!');
        }
        return redirect(route('list_admin_path'));
    }

    public function edit(Admin $admin){
        // $admin = $admin->find($id);
        return view('admin.admin.admin_edit',compact('admin'));
    }
    public function update(Request $request,Admin $admin){
    	$validateArray = array(
    		'first_name' => 'required',
    		'last_name' => 'required',
     		'email' => 'required|email'
    	);
    	if($request->has('chngpassbtn')){
    		$validateArray['password'] = 'required';
    	}
    	$this->validate(request(),$validateArray);
        // $admin = $admin->find($id);
        $data = array(
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email
        );
        if($request->has('chngpassbtn')){
            $data['password'] =Hash::make($request->password);
        }
        if($admin->fill($data)->save()){
            Session::flash('success_msg', 'Admin details updated successfully!');
        }else{
            Session::flash('error_msg', 'Failed to update admin details!');
        }
        return redirect(route('list_admin_path'));
    }
     public function deleteAdmin(Request $request,Admin $admin){
        if($request->ajax()) {
            $admin = $admin->find($request->input('data'));
            if ($admin->delete()) {
                return 1;
            }
        }else{
        	return 0;
        }
    }
    public function paginate(Request $request){
    	$admins = Admin::select([DB::raw('CONCAT(first_name, " ", last_name) AS full_name'),'email','status','id'])->where('id','!=',Auth::guard('admin')->User()->id);
        $datatables =  Datatables::of($admins);
        $keyword = \Request::get('search')['value'];
        $datatables->filterColumn('full_name', 'whereRaw', "CONCAT(first_name,' ',last_name) like ?", ["%$keyword%"]);
        $datatables ->filterColumn('email', 'whereRaw', "email like ?", ["%$keyword%"]);
        $datatables ->filterColumn('status', 'whereRaw', "status = ?", ["$keyword"]);
        $datatables->setRowId('id');
        return $datatables->make(true);
    }
    public function ExportToExcel(){
        $admins_data = Admin::select([DB::raw('CONCAT(first_name, " ", last_name) AS Name'),'email as Email','status as Status','created_at as Date'])->get()->toArray();
        Excel::create('Admins('.date('Y-m-d').")", function($excel) use($admins_data) {
            $excel->sheet('first sheet', function ($sheet) use ($admins_data) {
                $sheet->fromArray($admins_data);
            });
        })->export('csv');
    }
}
