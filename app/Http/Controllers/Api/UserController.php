<?php

namespace App\Http\Controllers\Api;

use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Validator;
use Illuminate\Support\Facades\Response;

class UserController extends ApiController
{
    use AuthenticatesUsers;
	protected $userTransformer;

    /**
     * UserController constructor.
     * @param $userTransformer
     */
    public function __construct(UserTransformer $userTransformer)
    {
        $this->userTransformer = $userTransformer;
    }

    public function signup(Request $request){
         $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            $errorMessage = '';
            foreach ($validator->messages()->all() as $key => $value) {
                $errorMessage .= $value."\n";
            }
            return $this->respond([
                'status' => 0,
                'message' => $errorMessage,
                'data' => array()
            ]);
        }
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'api_token' => str_random(60)
        ]);
    	return $this->respond([
            'status' => 1,
            'message' => 'User registerd successfully!',
            'data' => $this->userTransformer->transform($user->toArray())
        ]);
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            $errorMessage = '';
            foreach ($validator->messages()->all() as $key => $value) {
                $errorMessage .= $value."\n";
            }
            return $this->respond([
                'status' => 0,
                'message' => $errorMessage,
                'data' => array()
            ]);
        }
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $user = Auth::user();
            $user->api_token = str_random(60);
            $user->save();
            // dd(User::all()->toArray());
           return $this->respond([
            'status' => 1,
            'message' => 'Login successfully!',
            'data' => $this->userTransformer->transform($user->toArray())
        ]);
        }else{
            return $this->respond([
                'status' => 0,
                'message' => 'Email or password are wrong',
                'data' => array()
            ]);
        }
    }

    public function contatcus(){
        dd('yes its after auth');
    }

    public function logout(){
        $user = \Auth::guard('api')->user();
        $user->api_token = '';
        $user->save();
        return $this->respond([
            'status' => 1,
            'message' => 'Logged out successfully!',
            'data' => array()
        ]);
    }
}
