<?php

namespace App\Http\Controllers\Api;

use App\Transformers\TestTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Test;
use Illuminate\Support\Facades\Response;

class TestController extends ApiController
{
	protected $testTransformer;

    /**
     * TestController constructor.
     * @param $testTransformer
     */
    public function __construct(TestTransformer $testTransformer)
    {
        $this->testTransformer = $testTransformer;
    }


    public function index(){
    	 return $this->respond([
            'data' => $this->testTransformer->transformCollection(Test::all()->toArray())
        ]);
    }
}
