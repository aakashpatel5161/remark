<?php

use Illuminate\Database\Seeder;

class AdminRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$roles = [['role_name' => 'Super','added_date_time'=>gmdate('Y-m-d H:i:s')],['role_name' => 'Default','added_date_time'=>gmdate('Y-m-d H:i:s')],['role_name' => 'Provider','added_date_time'=>gmdate('Y-m-d H:i:s')]];
		DB::table('admin_roles')->insert($roles);
    }
}
