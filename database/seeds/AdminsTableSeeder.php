<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = ['role_id' => '1','first_name'=>'Niral','last_name'=>'last','email'=>'niral.bhalodia@openxcellinc.com','password'=>bcrypt('admin'),'added_date_time'=>gmdate('Y-m-d H:i:s')];
		DB::table('admins')->insert($admin);
    }
}
